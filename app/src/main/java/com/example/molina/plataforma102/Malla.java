package com.example.jorgerodriguez.plataforma102;

/**
 * Created by JorgeRodriguez on 22/8/2018.
 */

public class Malla {
    private int maxX;
    private int maxY;
    private float pixelHeight;
    private float pixelWidth;
    private float rHeight=10;
    private float rWidth=18;

    public Malla(int width, int height) {
        maxX = (width - 1);
        maxY = (height - 1);
        pixelWidth = (rWidth / maxX);
        pixelHeight = (rHeight / maxY);


    }

    public int Xi(float paramFloat){ return Math.round(paramFloat / pixelWidth); }
    public int Yi(float paramFloat){ return maxY - Math.round(paramFloat / pixelHeight); }
    public float fx(int paramInt) {return paramInt * pixelWidth;}
    public float fy(int paramInt) { return (maxY - paramInt) * pixelHeight; }


}
