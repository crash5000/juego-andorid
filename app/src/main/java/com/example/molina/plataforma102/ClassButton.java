package com.example.jorgerodriguez.plataforma102;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

/**
 * Created by JorgeRodriguez on 22/8/2018.
 */
/**************************************************************************************************/
/*  Esta clase se utiliza para pintar los elementos de control en la pantalla del dispositivo mobil
/**************************************************************************************************/
public class ClassButton {
    //Para el eje X
    private int minX;
    //Para el eje Y como
    private int minY;
    //Para el eje X como limite maximo
    private int maxX;

    private int maxY;
    private Drawable drawable;

    public ClassButton(GameView gameView, Drawable drawable, int minx, int miny, int maxx, int maxy){
        this.drawable =drawable;
        this.minX= minx;
        this.minY=miny;
        this.maxX=maxx;
        this.maxY=maxy;
    }
    public boolean checkButton(float x, float y){
        boolean touched =false;
        if((minY > y && maxY < y)&&(minX<x&&maxX>x)){
            touched=true;
        }
        return touched;
    }
    public void onDraw(Canvas canvas){
        // Rect dst = new Rect(minX, maxY, maxX , minY);
        //canvas.draw
        drawable.setBounds(minX, maxY, maxX , minY);
        drawable.draw(canvas);

    }
}

