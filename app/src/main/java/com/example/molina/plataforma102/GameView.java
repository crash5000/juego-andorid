package com.example.jorgerodriguez.plataforma102;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JorgeRodriguez on 22/8/2018.
 */

public class GameView extends SurfaceView {
    private List<Sprite> sprites = new ArrayList<Sprite>();
    private List<ClassButton> buttons = new ArrayList<ClassButton>();
    private int super_index=0;
    private Malla malla;
    private GameLoopThread gameLoopThread;
    private Paint paint;
    private Drawable d;
    private SurfaceHolder surfaceHolder;
    private long lastClick;
    private int vi_hamtaro_maxX = 7;
    public GameView(Context context) {
        super(context);

        gameLoopThread = new GameLoopThread(this);
        surfaceHolder=getHolder();
        surfaceHolder.addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                GenerarMalla();
                createSprites();
                createButtons();

                System.out.println(getHeight());
                System.out.println(getWidth());
                gameLoopThread.setRunning(true);
                gameLoopThread.start();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                boolean retry = true;
                gameLoopThread.setRunning(false);
                while (retry) {
                    try {
                        gameLoopThread.join();
                        retry = false;
                    } catch (InterruptedException e) {}
                }
            }
        });

        d = getResources().getDrawable(R.drawable.planetanamek);
        paint= new Paint();
    }

    private void createButtons(){
        buttons.add(createButton(R.drawable.flechaup,1,8,2,9));
        buttons.add(createButton(R.drawable.flechadown,1,6,2,7));
        buttons.add(createButton(R.drawable.flechal,0,7,1,8));
        buttons.add(createButton(R.drawable.flechar,2,7,3,8));
    }
    private ClassButton createButton(int resouce,int minx,int miny, int maxx ,int maxy){
        Drawable drawable =getResources().getDrawable(resouce);
        return new ClassButton(this,drawable ,malla.Xi(minx),malla.Yi(miny),malla.Xi(maxx),malla.Yi(maxy));
    }
    private void createSprites() { // necesitamos un identificador aki
        //sprites.add(createSprite(R.drawable.image62,1,1,3,3,5));
        //sprites.add(createSprite(R.drawable.image117,9,2,16,9,10));

        //Hamtaro (minY = altura minima, maxY altura maxima ; Maximo X desplazamiento derecha, Minimo desplazamiento Izquierda );
        sprites.add(createSprite(R.drawable.hamtaro1,vi_hamtaro_maxX,4,9,6,3));
    }
    private Sprite createSprite(int resouce,int minx,int miny, int maxx ,int maxy,int fotogramas) {
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), resouce);
        return new Sprite(this,bmp ,malla.Xi(minx),malla.Yi(miny),malla.Xi(maxx),malla.Yi(maxy),fotogramas);
    }

    private void GenerarMalla(){
        malla= new Malla(getWidth(),getHeight());
    }


    @Override
    protected void onDraw(Canvas canvas) {

        d.setBounds(malla.Xi(0),malla.Yi(10),malla.Xi(18),malla.Yi(0));
        d.draw(canvas);

        paint.setColor(Color.YELLOW);
        for(int i=0;i<10;i++) {
            canvas.drawLine(malla.Xi(i), malla.Yi(0), malla.Xi(i), malla.Yi(10), paint);
            canvas.drawLine(malla.Xi(0), malla.Yi(i), malla.Xi(10), malla.Yi(i), paint);
        }

        for (Sprite sprite : sprites) {
            sprite.onDraw(canvas);

        }

        for (ClassButton classButton : buttons) {
            classButton.onDraw(canvas);
        }

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (System.currentTimeMillis() - lastClick > 250) {
            lastClick = System.currentTimeMillis();
            boolean found= false;
            int index=0;
            outerloop:
            for (ClassButton classButton : buttons) {
                if(classButton.checkButton(event.getX(),event.getY())==true){
                    acction_Buttons(index);
                    found=true;
                    break outerloop;
                }
                index++;
            }
            if(found != true){
                index=0;
                for (Sprite sprite : sprites) {
                    if(sprite.checkSprite(event.getX(),event.getY())==true){
                        // System.out.println("yup sprite " + index);
                        sprite.selected(true);
                        super_index=index;
                    }else{
                        sprite.selected(false);
                    }
                    index++;
                }
            }
        }
        return true;
    }


    private void acction_Buttons(int index_buttons){
        switch (index_buttons) {
            case 0:  {
                Sprite s;
                s = sprites.get(super_index);
                s.Modificador_y(-50);
            }
            break ;
            case 1:  {
                Sprite s;
                s = sprites.get(super_index);
                s.Modificador_y(+50);
            }
            break ;
            case 2:  {
                Sprite s;
                s = sprites.get(super_index);
                s.Modificador_x(-50);
            }
            break ;
            case 3:  {
                Sprite s ;
                s = sprites.get(super_index);
                s.Modificador_x(+50);
            }
            break ;
            default: System.out.println("ningun boton prro");
        }
    }

}

