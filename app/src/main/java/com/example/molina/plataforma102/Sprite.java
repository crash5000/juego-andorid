package com.example.jorgerodriguez.plataforma102;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by JorgeRodriguez on 22/8/2018.
 */

public class Sprite {

    private int minX ;
    private int maxY ;
    private GameView gameView;
    private Bitmap bmp;
    private int currentFrame = 0;
    private int width;
    private int height;
    private int BMP_COLUMNS ;
    private int maxX;
    private int minY;
    private boolean selected=false;

    private int tnt_filas = 4;
    private int tnt_direccion = 0 ;

    public Sprite(GameView gameView, Bitmap bmp,int minx,int miny, int maxx, int maxy, int columnas) {
        //agregar seleccionable y cambiar fila
        this.gameView=gameView;
        this.bmp=bmp;
        this.BMP_COLUMNS=columnas;
        this.width=bmp.getWidth()/BMP_COLUMNS;
        this.height=bmp.getHeight()/tnt_filas;
        this.minX=minx;
        this.maxY=maxy;
        this.maxX=maxx;
        this.minY=miny;

    }
    public void Modificador_x(int modificador){
        //tope a la derecha
        if( gameView.getWidth() > maxX && minX > 0 ){
            //Entonces
            maxX=maxX+modificador;
            minX=minX+modificador;
        }else{
            maxX=maxX-1;
            minX=minX-1;
            if(gameView.getWidth() < maxX){
                maxX=maxX+modificador;
                minX=minX+modificador;
            }
        }


        if( modificador > 0 ) {
            //camina en el eje positivo de la X entonces la direccion es igual a 2
            tnt_direccion = 2;
        }else{
            tnt_direccion = 1;
        }

    }


    public  void Modificador_y(int modificador){
        maxY=maxY+modificador;
        minY=minY+modificador;
        if( modificador > 0 ) {
            //camina en el eje positivo de la X entonces la direccion es igual a 2
            tnt_direccion = 0;
        }else{
            tnt_direccion = 3;
        }
    }

    private void update() {
        currentFrame = ++currentFrame % BMP_COLUMNS;
    }

    public boolean checkSprite(float x, float y){
        boolean touched =false;
        if((minY > y && maxY < y)&&(minX<x&&maxX>x)){
            touched=true;
        }
        return touched;
    }
    public void selected(Boolean selecionado){
        this.selected=selecionado;
    }

    public void onDraw(Canvas canvas) {
        update();
        int srcX = currentFrame * width;
        int srcY = tnt_direccion * height;
        if(selected==true) {
            Paint localpaint = new Paint();
            localpaint.setColor(Color.WHITE);
            canvas.drawRect(minX, maxY, maxX, minY,localpaint);
        }
        Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
        Rect dst = new Rect(minX, maxY, maxX ,minY);
        canvas.drawBitmap(bmp, src, dst, null);
    }

}
